<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/hola-mundo', function () {
    return view('hola-mundo');
});

Route::get('/contacto/s', function () {
    return view('contacto');
});

Route::post('/hola-mundo', function () {
    return 'hola mundo post';
});

Route::match(['get', 'post'], '/contactos', function () {
    return 'contactos hola mundo post';
});

Route::get('/contacto/{nombre?}/{edad?}', function ($nombre = 'efrain', $edad = null) {

    //return view('contacto', ['nombre' => $nombre, 'edad' => $edad ]);
    return view('contacto.contacto')
        ->with('nombre', $nombre)
        ->with('edad', $edad)
        ->with('frutas', ['naranja', 'manzana', 'sandia', 'fresa', 'melon']);
})->where([
    'nombre' => '[A-Za-z]+',
    'edad' => '[0-9]+'
]);


Route::group(['prefix' => 'fruteria'], function (){
    Route::get('/frutas', 'FrutasController@getIndex');

    Route::get('/naranjas/{admin?}', [
        'middleware'=> 'EsAdmin',
        'uses' => 'FrutasController@getNaranjas',
        'as' => 'naranjitas'
    ]);

    Route::get('/peras', 'FrutasController@anyPeras');

});


Route::post('/recibir', 'FrutasController@recibirFormulario');

//Route::resource('frutas', 'FrutasController');

//Route::resource('notes', 'NotesController');

Route::get('/notes', 'NotesController@index');
Route::post('/notes', 'NotesController@store');
Route::get('/notes/create', 'NotesController@create');
Route::get('/notes/{id}', 'NotesController@show');
Route::get('/notes/{id}/edit', 'NotesController@edit');
Route::post('/notes/{id}', 'NotesController@update');//put
Route::get('/notes/destroy/{id}', 'NotesController@destroy');
//Route::delete('/notes/{id}', 'NotesController@destory');
