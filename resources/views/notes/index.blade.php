<h1>Aplicación de notas</h1>

<a href="{{ url('/notes/create')  }}">Crear nota</a>

@if(session('status'))
    <p style="color: blue;">{{ session('status') }}</p>
@endif

<h3>Listado de notas</h3>
<ul>
@foreach($notes as $note)
    <li>
        <ul>
            <li>{{ $note->title }}</li>
            <li ><a href="{{ url('/notes/' . $note->id) }}">Ver</a></li>
            <li><a href="{{ url('/notes/' . $note->id .'/edit') }}">Editar</a></li>
            <li><a href="{{ url('/notes/destroy/' . $note->id) }}">Borrar</a></li>
        </ul>
    </li>



@endforeach
</ul>