<h1>
    @if(!isset($note))
        Crear una nota
    @else
        Editar nota: {{ $note->id }}
    @endif
</h1>


<form action="{{ isset($note) ?  url('/notes/' . $note->id) :  url('/notes') }}" method="post">
    <p>
        <input type="text" name="title" placeholder="Título de nota" value="{{ isset($note)? $note->title : '' }}">
    </p>
    <p>
        <textarea name="description" cols="30" rows="10">{{ isset($note)? $note->description : '' }}</textarea>
    </p>
    <input type="submit" value="{{ isset($note) ? 'Guardar cambios' : 'Guardar' }}">
</form>

<a href="{{ url('/notes') }}">Volver a listado</a>