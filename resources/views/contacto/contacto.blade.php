
{{-- Comentario --}}
{{--<h1>Nombre {{$nombre}} Edad {{$edad}}</h1>--}}

{{--<h1>Nombre {!!$nombre!!} Edad {!! $edad !!}</h1>--}}

@include('contacto.cabecera', ['nombre' => $nombre])


<h1>Nombre {{$nombre}} Edad {{isset($edad) && !is_null($edad)? $edad : 'no existe la edad' }}</h1>

{{-- Condicional --}}
@if(is_null($edad))
    No exite la edad
@else
    Si existe la edad: {{$edad}}
@endif

{{-- Bucles --}}
<br/> Ciclo for
<?php $numero = 6; ?>
Tabla del {{ $numero  }} <br/>
@for($i = 1; $i<= 10; $i++)
    {{$i . ' x ' . $numero . ' = ' . ($i*$numero) }}<br/>
@endfor


Ciclo while
<?php $a = 1  ?>
@while($a < 7)
    {{ 'ciclo: ' . $a }}
    <?php $a++ ?>
@endwhile
<br/>
Ciclo forech
<h1>Listado de Frutas</h1>
@foreach($frutas as $fruta)
    <p>{{ $fruta  }}</p>
@endforeach