
<h1>Listado de Frutas (Accion del controlador)</h1>

<a href="{{ route('naranjitas')  }}">Ir a naranjas</a>
<a href="{{ action('FrutasController@anyPeras')  }}">Ir a peras</a>
<ul>
@foreach($frutas as $fruta)
     <li>
        <p>{{ $fruta  }}</p>
     </li>
@endforeach
</ul>


<h1>Formulario en laravel</h1>
<form action="{{ url('/recibir')  }}" method="post">
    <p>
        <label for="nombre">Nombre de la fruta</label>
        <input type="text" name="nombre" />
    </p>

    <p>
        <label for="descripcion">Descripcion de la fruta</label>
        <textarea type="text" name="descripcion"></textarea>
    </p>

    <input type="submit" value="Enviar">
</form>