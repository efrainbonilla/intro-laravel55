<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });*/

        DB::statement("
            CREATE TABLE notes
            (
                id SERIAL PRIMARY KEY,
                title VARCHAR(200),
                description TEXT,
                created_at TIMESTAMP,
                updated_at TIMESTAMP
            );
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::dropIfExists('notes');*/
    }
}
