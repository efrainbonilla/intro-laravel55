<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProbandosqlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::create('probandosql', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });*/

        DB::statement("
            CREATE TABLE probandosql
            (
                id SERIAL PRIMARY KEY,
                publication VARCHAR(200),
                description TEXT
                
            );
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::dropIfExists('probandosql');*/
    }
}
