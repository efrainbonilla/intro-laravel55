<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotesController extends Controller
{
    //
    public function index() {

        // conseguir todas las notas
        $notes = DB::table('notes')->orderBy('id', 'DESC')->get();# ->first();

        return view('notes.index', ['notes' => $notes]);
    }

    public function create() {

        return view('notes.create');
    }

    public function store(Request $request) {
        $note = DB::table('notes')->insert([
            'title' => $request->input('title'),
            'description' => $request->input('description')
        ]);

        return redirect()->action('NotesController@index');
    }

    public function show($id) {
        //conseguir una nota
        //$note = DB::table('notes')->where(['id' => $id]);

        $note = DB::table('notes')->select('title', 'description')->where('id', $id)->first();//solo las columnas necesarias

        if (empty($note)) {
            return redirect()->action('NotesController@index');
        }

        return view('notes.note', ['note' => $note]);
    }


    public function edit($id) {
        $note = DB::table('notes')->where('id', $id)->first();

        return view('notes.create', ['note' => $note]);
    }

    public function update($id, Request $request) {
        $note = DB::table('notes')->where('id', $id)->update([
           'title' => $request->input('title'),
            'description' => $request->input('description')
        ]);

        return redirect()->action('NotesController@index')->with('status', 'Nota actualizado correctamente.');
    }

    public function destroy($id) {

        $note = DB::table('notes')->where('id', $id)->delete();

        return redirect()->action('NotesController@index')->with('status', 'Nota borrado correctamente.');
    }
}
