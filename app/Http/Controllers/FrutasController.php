<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrutasController extends Controller
{
    //Accion que devuelva una vista
    public function getIndex() {
        return view('frutas.index')
            ->with('frutas', ['naranja', 'manzana', 'sandia', 'fresa', 'melon']);
    }

    public function getNaranjas() {
        return 'Acción de naranjas';
    }

    public function anyPeras($param1 = null, $param2 = null){
        return 'Acción de peras'. $param1 . ' ' . $param2;
    }

    public function recibirFormulario(Request $request) {
        //$data = $request->post();


        //return 'El nombre de la fruta es: ' . $request['nombre'];
        return 'El nombre de la fruta es: ' . $request->input('nombre');
    }

}
